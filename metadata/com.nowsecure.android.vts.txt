Categories:Security
License:MIT
Web Site:
Source Code:https://github.com/nowsecure/android-vts
Issue Tracker:https://github.com/nowsecure/android-vts/issues

Auto Name:VTS for Android
Summary:Check for vulnerabilities
Description:
This tool was meant to show the end user the attack surface that a given device
is susceptible to. In implementing these checks we attempt to minimize or
eliminate both false positives/false negatives without negatively affecting
system stability.  List of current bug checks:

* ZipBug9950697
* Zip Bug 8219321 / Master keys
* Zip Bug 9695860
* Jar Bug 13678484 / Android FakeID
* CVE 2013-6282 / put/get_user
* CVE_2011_1149 / PSNueter / Ashmem Exploit
* CVE_2014_3153 / Futex bug / Towelroot
* CVE 2014-3847 / WeakSauce * StumpRoot
* Stagefright bugs
* x509 Serialization bug
* PingPong root - CVE-2015-3636
* Stagefright - CVE-2015-6602
* Samsung Remote Code Execution as System
* CVE-2015-6608
.

Repo Type:git
Repo:https://github.com/nowsecure/android-vts.git

Build:v.6,6
    disable=binaries
    commit=v.6
    subdir=app
    gradle=yes

Build:v.9,9
    commit=v.9
    subdir=app
    gradle=yes

Build:v.11,11
    commit=v.11
    subdir=app
    gradle=yes

Build:v.12,12
    commit=v.12
    subdir=app
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:v.12
Current Version Code:12
