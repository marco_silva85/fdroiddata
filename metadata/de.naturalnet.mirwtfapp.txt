Categories:Science & Education
License:MirOS
Web Site:https://www.mirbsd.org/wtf.htm
Source Code:https://github.com/Natureshadow/MirWTFApp
Issue Tracker:https://github.com/Natureshadow/MirWTFApp/issues

Auto Name:WTF‽ (The MirOS Project)
Summary:Offline acronym translator
Description:
The WTF app is an offline version of the wtf acronyms database frontend as
shipped with The MirOS Project.

It uses the acronyms database maintained by The MirOS project, which at the time
of writing knew about more than 19000 acronyms.
.

Repo Type:git
Repo:https://github.com/Natureshadow/MirWTFApp.git

Build:0.2,4
    commit=0.2
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:0.2
Current Version Code:4
